#!/bin/bash

#
export WORKSPACE=/scratch/ws/0/apku868a-easybuild

# Module environment
module load modenv/scs5

#
module load EasyBuild

#
export EASYBUILD_ALLOW_LOADED_MODULES=EasyBuild,modenv/scs5
export EASYBUILD_DETECT_LOADED_MODULES=unload
export EASYBUILD_BUILDPATH="/tmp/${USER}-EasyBuild${SLURM_JOB_ID:-}"
export EASYBUILD_SOURCEPATH="${WORKSPACE}/sources"
export EASYBUILD_INSTALLPATH="${WORKSPACE}/easybuild-$(basename $(readlink -f /sw/installed))"
export EASYBUILD_INSTALLPATH_MODULES="${EASYBUILD_INSTALLPATH}/modules"
module use "${EASYBUILD_INSTALLPATH_MODULES}/all"
export LMOD_IGNORE_CACHE=1

eb $1 -r

