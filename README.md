# Easybuild for ZIH Systems
This repository contains EasyBuild configuration data which can be used to install software/frameworks on HPC Systems that are using [EasyBuild](https://github.com/easybuilders/easybuild) framework.

## Software Installation

---

File [easybuild-setup.sh](./easybuild-setup.sh) can be used to initialize/install frameworks/software on ZIH System (if it not installed by administrators). It is based on information on [ZIH HPC Compedium - Easybuild](https://doc.zih.tu-dresden.de/software/custom_easy_build_environment/).

### Setup

```bash
# Set user created workspace where software will be installed
export WORKSPACE=...
```

### Usage

```bash
# Create a job where user wants to work on the software. Don't forget to add
# '--pty' and 'bash -l' to get the command line back after initializing job. Eg:
srun --partition=haswell --nodes=2 --time=01:00:00 --pty bash -l

# Run easybuild-setup.sh with easybuild config file
source easybuild-setup.sh <easybuild-config-file>
```

## Easybuild Configurtions

---

Currently there are easybuild configuration files for following software/frameworks:
- [Apache Flink](https://flink.apache.org/)
  - v1.14.2: [Configuration file](./easybuild-configurations/Flink/1.14.2/)
  - v1.12.3: [Configuration file](./easybuild-configurations/Flink/1.12.3/) (Creator: [Jan Frenzel](https://gitlab.hrz.tu-chemnitz.de/s2817051--tu-dresden.de))
- [Apache Kafka](https://kafka.apache.org/) 
  - v3.0.0: [Work in progress]()

